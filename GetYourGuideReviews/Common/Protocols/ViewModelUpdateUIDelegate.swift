//
//  ViewModelUpdateUIDelegate.swift
//  GetYourGuideReviews
//
//  Created by Olla Ashour on 2/25/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import Foundation

protocol ViewModelUpdateUIDelegate {
    func willBeginDataRequests()
    func didCompleteDataRequests(messageStatus: String?)
}
