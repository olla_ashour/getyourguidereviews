//
//  AbstractViewModel.swift
//  GetYourGuideReviews
//
//  Created by Olla Ashour on 2/25/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import Foundation

class AbstractViewModel: NSObject {
    
    var apiClient: APIClient
    var delegate: ViewModelUpdateUIDelegate?
    
    override init() {
        apiClient = APIClient()
    }
}
