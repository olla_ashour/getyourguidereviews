//
//  APIClient+Reviews.swift
//  GetYourGuideReviews
//
//  Created by Olla Ashour on 2/25/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import Foundation

extension APIClient {
    
    func getReviews(params: [String: Any]?, completion: @escaping (TotalReviews?, String?) -> Void) {
        
        sendRequest(url: "https://www.getyourguide.com/berlin-l17/tempelhof-2-hour-airport-history-tour-berlin-airlift-more-t23776/reviews.json", method: .get, headers: nil, parameters: params) { ( data, error) in
           
            guard let data = data else {
                completion(nil, error?.localizedDescription)
                return
            }
            do {
                //TODO: Needs improvement
                let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let reqJSONString = String(data: jsonData, encoding: .utf8)
                if let responseData = reqJSONString?.data(using: .utf8) {
                    let decoder = JSONDecoder()
                    let reviews = try decoder.decode(TotalReviews.self, from: responseData)
                    completion(reviews, nil)
                }
                
            } catch let err {
                completion(nil, "Can't Parse Data due to" + err.localizedDescription)
            }
        }
    }
}
