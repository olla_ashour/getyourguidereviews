//
//  APIClient.swift
//  GetYourGuideReviews
//
//  Created by Olla Ashour on 2/25/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import Foundation
import Alamofire

class APIClient {
    
    init() {
    }
    
    func sendRequest(url: String, method: HTTPMethod, headers: HTTPHeaders?, parameters: [String: Any]? = nil, completion: @escaping (Any?, Error?) -> Void) {
        let encoding: ParameterEncoding
        if method == .post {
            encoding = JSONEncoding.default
        } else {
            encoding = URLEncoding.default
        }
        Alamofire.request(url, method: method, parameters: parameters , encoding:encoding, headers: headers).responseJSON{ (response) in
            
            switch response.result {
            case .success(let data):
                completion(data, nil)
            case .failure:
                completion(nil, response.error)
            }
            
        }
    }
}
