//
//  TotalReviews.swift
//  GetYourGuideReviews
//
//  Created by Olla Ashour on 2/25/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import Foundation

class TotalReviews: Codable {
    
    var totalReviewCommentsCount: Int
    var reviews: [Review] = []
    
    private enum CodingKeys: String, CodingKey {
        case totalReviewCommentsCount = "total_reviews_comments"
        case reviews = "data"
    }
}
