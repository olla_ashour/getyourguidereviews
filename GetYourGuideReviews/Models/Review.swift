//
//  Review.swift
//  GetYourGuideReviews
//
//  Created by Olla Ashour on 2/25/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import Foundation
import AvatarImageView

struct Review: Codable, AvatarImageViewDataSource {
    
    var reviewID: Int
    var rating: String
    var title: String?
    var message: String
    var author: String
    var date: String
    var languageCode: String
    var travelerType: String?
    var reviewerName: String
    var reviewerCountry: String
    var reviewerProfilePhoto: String?
    var name: String
    var isAnonymous: Bool
    var avatar: UIImage?

    private enum CodingKeys: String, CodingKey {
        case rating
        case title
        case message
        case author
        case date
        case languageCode
        case reviewerName
        case reviewerCountry
        case reviewerProfilePhoto
        case name = "firstInitial"
        case isAnonymous
        case reviewID = "review_id"
        case travelerType = "traveler_type"
        
    }
    
    
    
}
