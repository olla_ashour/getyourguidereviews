//
//  ReviewsViewController.swift
//  GetYourGuideReviews
//
//  Created by Olla Ashour on 2/22/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import UIKit
import SDWebImage

class ReviewsViewController: UIViewController {

    @IBOutlet weak var totalReviewsCountLabel: UILabel!
    @IBOutlet weak var reviewsTableView: UITableView!
    @IBOutlet weak var progressIndicator: UIActivityIndicatorView!
    
    var viewModel = ReviewsViewModel()
    var currentPage = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        viewModel.delegate = self
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getData()
    }

}

// MARK: TableView DataSource and Delegate Methods
extension ReviewsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getCurrentPageReviewsCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        if let reviewCell = tableView.dequeueReusableCell(withIdentifier: "ReviewsTableViewCell", for: indexPath) as? ReviewsTableViewCell {
            
            if let review = viewModel.getReview(at: indexPath.row) {
                reviewCell.configureCell(review: review)
            }
           
            return reviewCell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == viewModel.getCurrentPageReviewsCount() - 1 {
            currentPage += 1
            getData()
        }
    }
    
}

// MARK: ViewModelUIDelegate
extension ReviewsViewController: ViewModelUpdateUIDelegate {
   
    func willBeginDataRequests() {
         showProgressIndicator(true)
    }
    
    func didCompleteDataRequests(messageStatus: String?) {
        
        showProgressIndicator(false)
        
        if let totalReviewsCount = viewModel.getTotalReviewsCount() {
            totalReviewsCountLabel.text = String(totalReviewsCount)
        }
        
        reloadData()
    }
    
}

// MARK: ReviewsViewController Private Helpers
private extension ReviewsViewController {
    
    func configureTableView() {
        reviewsTableView.register(UINib(nibName: "ReviewsTableViewCell", bundle: nil), forCellReuseIdentifier: "ReviewsTableViewCell")
        reviewsTableView.estimatedRowHeight = 50.0
        reviewsTableView.rowHeight = UITableView.automaticDimension
    }
    
    func reloadData() {
        reviewsTableView.reloadData()
    }
    
    func showProgressIndicator(_ show: Bool) {
        progressIndicator.isHidden = !show
        if show {
            progressIndicator.startAnimating()
        } else {
            progressIndicator.stopAnimating()
        }
    }

    func getData() {
         viewModel.retrieveReviews(for: currentPage)
    }

}

