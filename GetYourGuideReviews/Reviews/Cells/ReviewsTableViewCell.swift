//
//  ReviewsTableViewCell.swift
//  GetYourGuideReviews
//
//  Created by Olla Ashour on 2/25/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import UIKit
import Cosmos
import AvatarImageView
import SDWebImage

class ReviewsTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var reviewerImage: AvatarImageView! {
        didSet {
            configureRoundAvatar()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        resetData()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        reviewerImage.layer.cornerRadius = reviewerImage.layer.frame.width / 2
        reviewerImage.layer.masksToBounds = true
    }
    
    func configureCell(review: Review) {
        if let title = review.title {
            titleLabel.text = "\"" + title + "\""
        } else {
            titleLabel.isHidden = true
            titleTopConstraint.constant = 0
        }
       
        messageLabel.text = review.message
        ratingView.rating = Double(review.rating) ?? 0.0
        
        authorLabel.text = review.isAnonymous ? "a GetYourGuide customer - " + review.reviewerCountry : review.author
        
        dateLabel.text = review.date
        
        if let photoURL = review.reviewerProfilePhoto {
            reviewerImage.sd_setImage(with: URL(string: photoURL), placeholderImage: nil, options: .refreshCached, completed: nil)
        }else {
            reviewerImage.dataSource =  review
        }
        
    }
    
}

// MARK : Private Helpers
private extension ReviewsTableViewCell {
    
    func configureRoundAvatar() {
        struct Config: AvatarImageViewConfiguration { var shape: Shape = .circle }
        reviewerImage.configuration = Config()
    }
    
    func resetData() {
        titleLabel.text = nil
        messageLabel.text = nil
        ratingView.rating = 0
        dateLabel.text = nil
        reviewerImage.image = nil
        authorLabel.text = nil
        titleTopConstraint.constant = 10
    }
    
}
