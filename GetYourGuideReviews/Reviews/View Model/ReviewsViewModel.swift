//
//  ReviewsViewModel.swift
//  GetYourGuideReviews
//
//  Created by Olla Ashour on 2/25/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import Foundation

class ReviewsViewModel: AbstractViewModel {
    
    private var params: [String: Any] = ["count": 20, "rating":0 , "sortBy": "date_of_review", "direction": "DESC"]
    
    private var totalReviews: TotalReviews?
    
    func retrieveReviews(for page: Int) {
        
        delegate?.willBeginDataRequests()
       
        params["page"] = page
        
        apiClient.getReviews(params: params) { (currentPageReviews, errorMessage) in
           
            //For paging
            if let totalReviews = self.totalReviews, let retrievedPageReviews = currentPageReviews  {
                totalReviews.reviews += retrievedPageReviews.reviews
            } else {
                self.totalReviews = currentPageReviews
            }
           
            self.delegate?.didCompleteDataRequests(messageStatus: "Success")
        }
    }
    
    func getCurrentPageReviewsCount() -> Int {
        return totalReviews?.reviews.count ?? 0
    }
    
    func getReview(at index: Int) -> Review? {
        return totalReviews?.reviews[index]
    }
    
    func getTotalReviewsCount() -> Int? {
        return totalReviews?.totalReviewCommentsCount
    }
    
    
}

