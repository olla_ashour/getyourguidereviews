A demo example that displays the list of reviews from one of GetYourGuide's Berlin Tours.

To run: Simply download the code and run on any simulator or any iOS version 9.0 and above.